package by.iba.printingparser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import by.iba.printingparser.validation.Validator;

/**
 * @author HALVIDZIS_MO
 */

public class Parser {

    private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);

    private static final String NEXT_LINE = "\r\n";
    private static final String TRANSF = "\r\n\r\n";
    private static final String TAB = "\t";
    private static final String DOUBLE_TAB = "\t\t";
    private static final String EMPTY_STRING = "";
    private static final String STRING_VALUE = "value";
    private static final String CALENDAR_VALUE = "(value)";
    private static final String END_CLASS = NEXT_LINE + "}" + NEXT_LINE;
    private static final String PACKAGE_NAME = "package %s;" + TRANSF;
    private static final String IMPORTS = "import com.sbsa.mca.csf.card.ams.eap.print.DocumentGenerationDataBuilder;"
            + NEXT_LINE
            + "import com.sbsa.mca.csf.output.management2.generation.keyvalue.KeyValueFile;" +
            TRANSF + "import java.util.Calendar;" + NEXT_LINE +
            "import java.util.function.Consumer;" + TRANSF;
    private static final String FIELDS = TAB + "private static final String %s = \"%s\";" + NEXT_LINE;
    private static final String METHOD_FIELDS = TRANSF + TAB + "public %s %s(%s value) {" + NEXT_LINE +
            DOUBLE_TAB + "return addField(%s, %s%s);" + NEXT_LINE + TAB + "}";
    private static final String METHOD_RECORDS = TRANSF + TAB + "public %s %s(Consumer<%s> filler) {" + NEXT_LINE +
            DOUBLE_TAB + "return addOrAugmentRecord(%s, filler);" + NEXT_LINE + TAB + "}";
    private static final String BEGIN_CLASS = "public class %s extends DocumentGenerationDataBuilder<%s> {" + TRANSF;
    private static final String CONSTRUCTOR = NEXT_LINE + TAB + "public %s() {" + NEXT_LINE + DOUBLE_TAB +
            "super(new KeyValueFile(%s));" + NEXT_LINE + TAB + "}";
    private static final String FIELD_METHOD_STRING = "String";
    private static final String FIELD_METHOD_CALENDAR = "Calendar";
    private static final Pattern PATTERN = Pattern.compile("(_[a-z])");
    private static final String[] NODE_TAG = new String[] { "FILE", "REC", "FLD" };
    private static final String HAS_TIME = "TIME";
    private static final String HAS_DATE = "DATE";
    private static final String TAG_NAME = "NAME";
    private static final String MISTAKE = "Parse with mistakes!";
    private static final String NODE_MISTAKE = "Parse with mistakes because of nodes!";
    private static final String SUCCESS = "Parse successfully!";
    private static final String FILE_CREATED = "File has been created";
    private static final String FILE_EXIST = "File already exist, and it will be rewriting";
    private static final String UNKNOWN_PACKAGE = "some.package ";
    private static final String EXTENSION = ".java";
    private String className = "";

    public String parse(File inputFile, File outputFile, String dateValue, String timeValue) {
        try {

            String packages = outputFile.getCanonicalPath().substring(3);
            packages = packages.replace(outputFile.getName(), "");
            packages = packages.replace("\\", ".");
            if (packages.length() <= 0) {
                packages = UNKNOWN_PACKAGE;
            }
            className += outputFile.getName().replace(EXTENSION, "");

            try {
                outputFile.getParentFile().mkdirs();
                boolean created = outputFile.createNewFile();
                if (created) {
                    LOGGER.info(FILE_CREATED + ". Line: ");
                } else {
                    LOGGER.info(FILE_EXIST + ". Line: ");
                }
            } catch (IOException ex) {
                LOGGER.error("IOException while creating new file was catch. Line: ", ex);
                return MISTAKE;
            }
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            ArrayList<Node> nList = new ArrayList<>();
            for (String s : NODE_TAG) {
                for (int i = 0; i < doc.getElementsByTagName(s).getLength(); i++) {
                    nList.add(doc.getElementsByTagName(s).item(i));
                }
            }

            nList = correct(nList);

            nList = deleteSameNode(nList);
            if (nList.size() == 0) {
                LOGGER.error("XML file has no \"FILE\", \"REC\", \"FLD\" tags. Line: ");
                return NODE_MISTAKE;
            }
            LOGGER.info("Nodes have found in XML file. Line: ");

            try (FileWriter writer = new FileWriter(outputFile)) {

                writer.write(String.format(PACKAGE_NAME, packages.substring(0, packages.length() - 1)));
                writer.write(IMPORTS);
                writer.write(String.format(BEGIN_CLASS, className, className));

                for (int i = 0; i < nList.size(); i++) {
                    Node node = nList.get(i);
                    writer.write(concat(node.getAttributes().getNamedItem(TAG_NAME).getNodeValue(),
                            NODE_TAG[1].equals(node.getNodeName()) && i == 1));
                }

                for (int i = 0; i < nList.size(); i++) {
                    Node node = nList.get(i);
                    String value = node.getAttributes().getNamedItem(TAG_NAME).getNodeValue();
                    if (NODE_TAG[0].equals(node.getNodeName())) {
                        writer.write(String.format(CONSTRUCTOR, className, value));
                    }
                    if (NODE_TAG[1].equals(node.getNodeName())) {
                        writer.write(concat(camel(value.toLowerCase()), value));
                    }
                    if (NODE_TAG[2].equals(node.getNodeName())) {
                        writer.write(
                                concat(camel(value.toLowerCase()), determine(node, dateValue, timeValue), value));
                    }
                }

                writer.write(END_CLASS);
                writer.flush();
                LOGGER.info("Writing in file was successfull! Line: ");
            } catch (IOException ex) {
                LOGGER.error("IOException while writing in file was catch. Line: ", ex);
                return MISTAKE;
            }

        } catch (Exception e) {
            LOGGER.error("Exception was catch. Line: ", e);
            return MISTAKE;
        }
        return SUCCESS;
    }

    private ArrayList<Node> deleteSameNode(ArrayList<Node> list) {
        for (int k = 0; k < list.size(); k++) {
            Node node = list.get(k);
            for (int i = k + 1; i < list.size(); i++) {
                if (node.getAttributes().getNamedItem(TAG_NAME)
                        .getNodeValue().equals(list.get(i).getAttributes().getNamedItem(TAG_NAME).getNodeValue())) {
                    list.remove(node);
                }
            }
        }
        return list;
    }

    private String concat(String str1, String[] str2, String str3) {
        return String.format(METHOD_FIELDS, className, str1, str2[0], str3, str2[1],
                (EMPTY_STRING.equals(str2[1])) ? STRING_VALUE : CALENDAR_VALUE);

    }

    private String concat(String str1, String str2) {
        return String.format(METHOD_RECORDS, className, str1, className, str2);
    }

    private String concat(String str1, boolean b) {
        if (b) {
            return String.format(FIELDS, str1, change(str1));
        }
        return String.format(FIELDS, str1, str1);
    }

    private String change(String str) {
        String result = str.substring(0, 1);
        result += str.substring(1).toLowerCase();
        return result.replaceAll("_", " ");
    }

    private String[] determine(Node node, String dateValue, String timeValue) {
        String value = node.getAttributes().getNamedItem(TAG_NAME).getNodeValue();
        if (value != null) {
            boolean isDate;
            if (value.indexOf(HAS_DATE) > -1 && dateValue != null) {
                isDate = true;
                return new String[] { FIELD_METHOD_CALENDAR, Validator.getMethod(node, dateValue, isDate) };
            } else if (value.indexOf(HAS_TIME) > -1 && timeValue != null) {
                isDate = false;
                return new String[] { FIELD_METHOD_CALENDAR, Validator.getMethod(node, timeValue, isDate) };
            }
        }
        return new String[] { FIELD_METHOD_STRING, EMPTY_STRING };
    }

    private String camel(String value) {
        Matcher matcher = PATTERN.matcher(value);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString().replaceAll("_", "");
    }

    private ArrayList<Node> correct(ArrayList<Node> list) {
        for (int i = 0; i < list.size(); i++) {
            Node node = list.get(i);
            String value = node.getAttributes().getNamedItem(TAG_NAME).getNodeValue().toUpperCase();
            list.get(i).getAttributes().getNamedItem(TAG_NAME).setNodeValue(value.replace(" ", "_"));
        }
        return list;
    }

}
