package by.iba.printingparser.validation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author HALVIDZIS_MO
 */
public class Validator {

    private static final Logger LOGGER = LoggerFactory.getLogger(Validator.class);

    private static final String AUTO_TIME = "TimeAuto";
    private static final String AUTO_DATE = "DateAuto";
    private static final String DATE_METHOD = "yyyyMMdd";
    private static final String TIME_METHOD = "hhmm";
    private static final String EMPTY_STRING = "";

    private static final String PROP_DATE_FILE_NAME = "date";
    private static final String PROP_TIME_FILE_NAME = "time";

    private ObservableList<String> result;

    public static String getMethod(Node node, String calendarValue, boolean isDate) {

        String value = "";
        try {
            value += node.getChildNodes().item(0).getTextContent();
            LOGGER.info("The calendar value is {}. Line: ", value);
        } catch (NullPointerException e) {
            LOGGER.error("NullPointerException was catch. Line: ", e);
            return EMPTY_STRING;
        }
        Validator validator = new Validator();
        if (isDate) {
            return validator.checkCalendarValue(value, calendarValue, AUTO_DATE, PROP_DATE_FILE_NAME, DATE_METHOD);
        } else {
            return validator.checkCalendarValue(value, calendarValue, AUTO_TIME, PROP_TIME_FILE_NAME, TIME_METHOD);
        }
    }

    public ObservableList<String> getCalendarProp(String fileName, boolean value) throws IOException {

        ResourceBundle rb = ResourceBundle.getBundle(fileName);

        List<String> list = new ArrayList<>();
        if (value) {
            for (String key : rb.keySet()) {
                list.add(rb.getString(key));
            }
        } else {
            list.addAll(Collections.list(rb.getKeys()));
        }

        result = FXCollections.observableArrayList(list);

        return result;
    }

    private String checkCalendarValue(String value, String calendarValue, String autoType, String fileName, String methodType) {
        Validator validator = new Validator();
        if (calendarValue != null && !autoType.equals(calendarValue)) {
            return methodType;
        } else if (calendarValue != null && autoType.equals(calendarValue)) {
            try {
                ObservableList<String> list = validator.getCalendarProp(fileName, true);
                for (String s : list) {
                    if (Pattern.matches(s, value)) {
                        return methodType;
                    }
                }
                LOGGER.info("The method type for calendar value is {}. Line: ", methodType);
            } catch (IOException e) {
                LOGGER.error("IOException was catch. Line: ", e);
                return EMPTY_STRING;
            }
        }
        return EMPTY_STRING;
    }
}
