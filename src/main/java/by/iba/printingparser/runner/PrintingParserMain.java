package by.iba.printingparser.runner;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.iba.printingparser.Parser;
import by.iba.printingparser.validation.Validator;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * @author HALVIDZIS_MO
 */

public class PrintingParserMain extends Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrintingParserMain.class);

    private static final double ELEMENT_HEIGHT = 35.0;
    private static final double BUTTON_WIDTH = 100.0;
    private static final double TEXT_AREA_WIDTH = 470.0;
    private static final double SCENE_HEIGHT = 300.0;
    private static final double SCENE_WIDTH = 600.0;
    private static final String NEW_TEXT_NAME = "Class.java";
    private static final String XML_FILES_FILTER = "ALL files (*.xml*)";
    private static final String XML_FILES_EXPANSION = "*.xml*";
    private static final String JAVA_FILES_FILTER = "ALL files (*.java*)";
    private static final String JAVA_FILES_EXPANSION = "*.java*";
    private static final String TEXT_ERROR_CHOOSING_XML = "You should choose XML file!";
    private static final String TEXT_ERROR_CHOOSING_JAVA = "You should choose path for generated file!";
    private static final String AUTO_TIME = "TimeAuto";
    private static final String AUTO_DATE = "DateAuto";
    private static final boolean KEYS = false;
    private static final String PROP_DATE_FILE_NAME = "date";
    private static final String PROP_TIME_FILE_NAME = "time";

    private File fileXML;
    private File fileJava;
    private String pathsInfoXML = "";
    private String pathsInfoJava = "";

    private ComboBox<String> timeComboBox;
    private ComboBox<String> dateComboBox;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {

        stage.setTitle("Parser");

        Validator validator = new Validator();
        TextArea filePathXML = new TextArea();
        TextArea filePathJava = new TextArea();
        filePathXML.setEditable(false);
        filePathXML.setPrefSize(TEXT_AREA_WIDTH, ELEMENT_HEIGHT);
        filePathJava.setEditable(false);
        filePathJava.setPrefSize(TEXT_AREA_WIDTH, ELEMENT_HEIGHT);
        Label labelError = new Label();
        Label labelFinish = new Label();
        try {
            timeComboBox = new ComboBox<>(validator.getCalendarProp(PROP_TIME_FILE_NAME, KEYS));
            dateComboBox = new ComboBox<>(validator.getCalendarProp(PROP_DATE_FILE_NAME, KEYS));
            LOGGER.info("Date and Time were find. Line: ");
        } catch (IOException e) {
            LOGGER.warn("IOException was catch because Date and Time were not find. Line: ", e);
            timeComboBox = new ComboBox<>();
            dateComboBox = new ComboBox<>();
        }
        timeComboBox.setValue(AUTO_TIME);
        dateComboBox.setValue(AUTO_DATE);

        /*
         * Button for opening XML file
         */
        Button btn1 = new Button();
        btn1.setPrefSize(BUTTON_WIDTH, ELEMENT_HEIGHT);
        btn1.setText("Open File");
        btn1.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                labelError.setText("");
                filePathXML.clear();
                FileChooser fileChooser = new FileChooser();

                // Set extension filter
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(XML_FILES_FILTER, XML_FILES_EXPANSION));

                // Show open file dialog
                fileXML = fileChooser.showOpenDialog(null);
                try {
                    pathsInfoXML += fileXML.getCanonicalPath();
                    LOGGER.info("XML file was found. Line: ");
                } catch (IOException ex) {
                    LOGGER.error("IOException was catch while choosing XML file! Line: ", ex);
                    pathsInfoXML = "";
                    labelError.setText(TEXT_ERROR_CHOOSING_XML);
                } catch (NullPointerException e) {
                    LOGGER.error("NullPointerException was catch while choosing XML file! Line: ", e);
                    pathsInfoXML = "";
                    labelError.setText(TEXT_ERROR_CHOOSING_XML);
                }
                filePathXML.setText(pathsInfoXML);

            }
        });

        /*
         * Button for saving generated file
         */
        Button btn2 = new Button();
        btn2.setPrefSize(BUTTON_WIDTH, ELEMENT_HEIGHT);
        btn2.setText("Save As");
        btn2.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                labelError.setText("");
                filePathJava.clear();
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Save generated file as...");
                // Set extension filter
                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(JAVA_FILES_FILTER,
                        JAVA_FILES_EXPANSION);
                fileChooser.getExtensionFilters().add(extFilter);
                fileChooser.setInitialFileName(NEW_TEXT_NAME);

                // Show open file dialog
                fileJava = fileChooser.showSaveDialog(stage);
                try {
                    pathsInfoJava += fileJava.getCanonicalPath();
                    LOGGER.info("Directory for saving Java file was choose. Line: ");
                } catch (IOException ex) {
                    LOGGER.error("IOException was catch while choosing directory for saving Java file! Line: ", ex);
                    pathsInfoJava = "";
                    labelError.setText(TEXT_ERROR_CHOOSING_JAVA);
                } catch (NullPointerException e) {
                    LOGGER.error("NullPointerException was catch while choosing directory for saving Java file! Line: ", e);
                    pathsInfoJava = "";
                    labelError.setText(TEXT_ERROR_CHOOSING_JAVA);
                }
                filePathJava.setText(pathsInfoJava);

            }
        });

        /*
         * Button for parsing XML file
         */
        Button btn3 = new Button();
        btn3.setPrefHeight(ELEMENT_HEIGHT);
        btn3.setText("Start parsing");
        btn3.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                if (filePathXML.getText().length() <= 3) {
                    labelError.setText(TEXT_ERROR_CHOOSING_XML);
                    LOGGER.warn("The XML file wasn't choose. Line: ");
                } else if (filePathJava.getText().length() <= 3) {
                    labelError.setText(TEXT_ERROR_CHOOSING_JAVA);
                    LOGGER.warn("The Java file directory wasn't choose. Line: ");
                } else {
                    labelError.setText("");
                    Parser parser = new Parser();
                    labelFinish.setText(parser.parse(fileXML, fileJava, dateComboBox.getValue(), timeComboBox.getValue()));
                    LOGGER.info(labelFinish.getText() + " Line: ");
                }
            }
        });

        VBox vBox = new VBox();
        vBox.setSpacing(10);
        HBox hButtonOpenBox = new HBox();
        hButtonOpenBox.setSpacing(10);
        hButtonOpenBox.getChildren().addAll(btn1, filePathXML);
        hButtonOpenBox.setAlignment(Pos.CENTER);
        HBox hButtonSaveBox = new HBox();
        hButtonSaveBox.setSpacing(10);
        hButtonSaveBox.getChildren().addAll(btn2, filePathJava);
        hButtonSaveBox.setAlignment(Pos.CENTER);
        HBox hCalendarBox = new HBox();
        hCalendarBox.setSpacing(10);
        hCalendarBox.getChildren().addAll(dateComboBox, timeComboBox);
        hCalendarBox.setAlignment(Pos.CENTER);
        HBox hParsingButtonBox = new HBox();
        hParsingButtonBox.setSpacing(10);
        hParsingButtonBox.getChildren().addAll(btn3, labelFinish);
        vBox.getChildren().addAll(hButtonOpenBox, hButtonSaveBox, labelError, hCalendarBox, hParsingButtonBox);
        StackPane root = new StackPane();
        root.setPadding(new Insets(10.0f));
        root.getChildren().add(vBox);
        stage.setScene(new Scene(root, SCENE_WIDTH, SCENE_HEIGHT));
        stage.show();
    }

}
