# printing-parser


This is the instruction for Parser app!

<img src="images/app.png" width="500">

Getting started
---------------

1) Click "Open File" button;

2) Choose .xml file on your PC or other device;

3) Click "Open";
- <img src="images/popupOpening.png" width="500">
   
4) Click "Save As" button;
- <img src="images/save.png" width="500">

5) Choose folder, where do you want to save generated .java file;

6) Click "Save";
- <img src="images/popupSaving.png" width="500">

7) In the dropdown menu "DateAuto", you can choose the format of the date in your .xml file (not necessary);
- <img src="images/date.png" width="500">

8) In the dropdown menu "TimeAuto", you can choose the format of time in your .xml file (not necessary);
- <img src="images/time.png" width="500">

9) Click "Start parsing" button;

Success
-------

1) If everything has been done successfully, you will see the message: "Parse successfully!", and find generated class by the path, you have been choose;
- <img src="images/start.png" width="500">

Errors
------

1) If you see the message "You should choose XML file!" it means that you forgot to do the steps 1-3!;
- <img src="images/errFile.png" width="500">

2) If you see the message "You should choose path for generated file!" it means that you forgot to do the steps 4-6!;
- <img src="images/errPath.png" width="500">

3) If you see the message "Parse with mistakes!" it means that there is a mistake in creating or writing in .java file or reading from .xml file;
- <img src="images/mistake.png" width="500">

4) If you see the message "Parse with mistakes because of nodes!" it means that there are no tags in the .xml file.
- <img src="images/nodeMistake.png" width="500">
